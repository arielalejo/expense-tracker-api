package com.ariel.expensetracker.constants;

public class Constants {
    public static final String JWT_PRIVATE_KEY = "insecurekey";
    public static final long JWT_TOKEN_VALIDITY = 2*60*60*1000;
}
