package com.ariel.expensetracker.filters;

import com.ariel.expensetracker.constants.Constants;
import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.services.AuthService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.impl.ClaimsHolder;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class AuthFilter extends GenericFilterBean {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        String authHeader= httpRequest.getHeader("x-auth");

        if (authHeader != null) {
            String token = this.extractToken(authHeader);
            if (token == null) {
                httpResponse.sendError(HttpStatus.UNAUTHORIZED.value(), "Auth token must be Bearer");
                return;
            }
            try{
                JWTVerifier verifier = JWT.require(AuthService.algorithm).build();
                DecodedJWT jwt = verifier.verify(token);


                httpRequest.setAttribute("userId", Integer.parseInt(jwt.getClaim("id").toString()));
                httpRequest.setAttribute("userName", jwt.getClaim("name").asString());

            } catch (JWTVerificationException exception){
                System.out.println(exception.getMessage());
                httpResponse.sendError(HttpStatus.UNAUTHORIZED.value(), "invalid/expired token");
                return;
            }

        } else{
            httpResponse.sendError(HttpStatus.BAD_REQUEST.value(), "no token provided");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }

    private String extractToken(String header) {
        String[] headerArr = header.split("Bearer ");
        if(headerArr.length > 1 && headerArr[1] != null){
            String token = headerArr[1];
            return token;
        } else {
            return null;
        }
    }
}
