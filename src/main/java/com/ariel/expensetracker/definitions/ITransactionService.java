package com.ariel.expensetracker.definitions;

import com.ariel.expensetracker.models.Transaction;

import java.util.List;

public interface ITransactionService {

    Transaction createTransaction(int userId, int categoryId, double amount, String note, long transactionDate);
    Transaction getTransactionById(int transactionId, int userId, int categoryId);
    List<Transaction> getAllTransactions(int userId, int categoryId);
    Transaction updateTransaction(int userId, int categoryId, int transactionId, Transaction transaction);
    void deleteTransactionById(int userId, int categoryId, int transactionId);
}
