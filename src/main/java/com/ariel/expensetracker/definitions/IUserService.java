package com.ariel.expensetracker.definitions;

import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;

public interface IUserService {
    String registerUser(String name, String lastName, String email, String password) throws AuthException;
    String validateUser(String email, String password) throws AuthException;

    User getUserById(int id);
}
