package com.ariel.expensetracker.definitions;

import com.ariel.expensetracker.exceptions.BadRequestException;
import com.ariel.expensetracker.exceptions.ResourceNotFoundException;
import com.ariel.expensetracker.models.Category;

import java.util.List;

public interface ICategoryRepository {
    List<Category> getAllCategories(int userId);
    Category getCategoryById(int userId, int categoryId) throws ResourceNotFoundException;
    int createCategory(int userId, String title, String description) throws BadRequestException;
    Category updateCategory(int userId, int categoryId, Category category) throws BadRequestException;
    void deleteCategory(int userId, int categoryId) throws ResourceNotFoundException;
}
