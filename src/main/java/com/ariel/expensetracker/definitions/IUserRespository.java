package com.ariel.expensetracker.definitions;

import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;

public interface IUserRespository {
    User registerUser(String name, String lastName, String email, String password) throws AuthException;
    User findByEmailAndPassword(String email, String password) ;
    User findById(int id) throws AuthException;
    int emailIsRegisteredInTable(String email);

}

