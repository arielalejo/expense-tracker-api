package com.ariel.expensetracker.repository;

import com.ariel.expensetracker.definitions.IUserRespository;
import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Repository("fakeUserRepository")
public class FakeUserRespository  {
    private Map<Integer, User> UsersDB = new HashMap<Integer, User>();


    public User registerUser(String name, String lastName, String email, String password) throws AuthException {
        int id = new Random().nextInt(1000);
        User user = new User(name, lastName, email, password);
        user.setId(id);

        UsersDB.put(id, user);
        return user;
    }


    public int findByEmailAndPassword(String email, String password) {
        int userId = this.findByEmail(email);
        if (userId == 0)   return 0;

        User user = UsersDB.get(userId);
        if (!password.equals(user.getPassword())) return 0;

        return user.getId();
    }


    public User findById(int id) throws AuthException {
        for(int userId: UsersDB.keySet()){
            if ( id == userId) return UsersDB.get(userId);
        }

        return null;
    }


    public int findByEmail(String email) {
        for(int userId: UsersDB.keySet()){
            System.out.print(userId);
            System.out.println(UsersDB.get(userId).getEmail());

            if(email.equals(UsersDB.get(userId).getEmail())) {
                return UsersDB.get(userId).getId();
            }

        }
        System.out.println("-----");

        return 0;
    }
}
