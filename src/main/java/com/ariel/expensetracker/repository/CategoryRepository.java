package com.ariel.expensetracker.repository;

import com.ariel.expensetracker.definitions.ICategoryRepository;
import com.ariel.expensetracker.exceptions.BadRequestException;
import com.ariel.expensetracker.exceptions.ResourceNotFoundException;
import com.ariel.expensetracker.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class CategoryRepository implements ICategoryRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_CREATE =
            "INSERT INTO et_categories(category_id, user_id, title, description) " +
            "VALUES(NEXTVAL('et_categories_seq'), ?, ?, ?)";
    private static final String SQL_FIND_BY_ID =
            "SELECT C.category_id, C.user_id, C.title, C.description, COALESCE(SUM(T.AMOUNT), 0) total_expense " +
            "FROM et_transactions T RIGHT OUTER JOIN et_categories C " +
            "ON C.category_id = T.category_id " +
            "WHERE C.user_id = ? AND C.category_id = ? GROUP BY C.category_id";
    private static final String SQL_FIND_ALL =
            "SELECT C.category_id, C.user_id, C.title, C.description, COALESCE(SUM(T.AMOUNT), 0) total_expense " +
                    "FROM et_transactions T RIGHT OUTER JOIN et_categories C " +
                    "ON C.category_id = T.category_id " +
                    "WHERE C.user_id = ? GROUP BY C.category_id";
    private static final String SQL_UPDATE =
            "UPDATE et_categories SET title = ?, description = ? " +
            "WHERE user_id = ? AND category_id = ?";
    private static final String SQL_DELETE =
            "DELETE FROM ET_CATEGORIES " +
            "WHERE USER_ID = ? AND CATEGORY_ID = ? ";

    @Override
    public List<Category> getAllCategories(int userId) {
        try{
            return jdbcTemplate.query(SQL_FIND_ALL, mapCategoryFromDB(), userId);

        }catch (Exception e){
            throw new BadRequestException("no categories for the given user");
        }

    }

    @Override
    public Category getCategoryById(int userId, int categoryId) throws ResourceNotFoundException {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, mapCategoryFromDB(), userId, categoryId);

        }catch (Exception e){
            throw new ResourceNotFoundException("no category with the given id");
        }

    }

    @Override
    public int createCategory(int userId, String title, String description) throws BadRequestException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);

                ps.setInt(1, userId);
                ps.setString(2, title);
                ps.setString(3, description);

                return ps;
            }, keyHolder);

            return  (Integer) keyHolder.getKeys().get("category_id");

        }catch(Exception e){
            System.out.println(e.getMessage());
            throw new BadRequestException("invalid data");
        }
    }

    @Override
    public Category updateCategory(int userId, int categoryId, Category category) throws BadRequestException {
        try {
            getCategoryById(userId, categoryId);

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_UPDATE, Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, category.getTitle());
                ps.setString(2, category.getDescription());
                ps.setInt(3, userId);
                ps.setInt(4, categoryId);

                return ps;
            }, keyHolder);

            return  new Category(
                    (Integer) keyHolder.getKeys().get("category_id"),
                    (Integer) keyHolder.getKeys().get("user_id"),
                    (String) keyHolder.getKeys().get("title"),
                    (String) keyHolder.getKeys().get("description"),
                    (Double) keyHolder.getKeys().get("total_expense")
            );
        }
        catch (Exception e){
            throw new BadRequestException("invalid data for update a category or inexistent category");
        }
    }

    @Override
    public void deleteCategory(int userId, int categoryId) throws ResourceNotFoundException {
        int row = jdbcTemplate.update(SQL_DELETE, userId, categoryId);

        if (row == 0) { throw new ResourceNotFoundException("No category with the given Id, No category was deleted");}
    }

    private RowMapper<Category> mapCategoryFromDB(){
        return (rs, i) -> {
            return new Category(
                    rs.getInt("category_id"),
                    rs.getInt("user_id"),
                    rs.getString("title"),
                    rs.getString("description"),
                    rs.getDouble("total_expense")
            );

        };
    }
}
