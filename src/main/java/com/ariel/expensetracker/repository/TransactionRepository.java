package com.ariel.expensetracker.repository;

import com.ariel.expensetracker.definitions.ITransactionRepository;
import com.ariel.expensetracker.exceptions.BadRequestException;
import com.ariel.expensetracker.exceptions.ResourceNotFoundException;
import com.ariel.expensetracker.models.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.JDBCType;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class TransactionRepository implements ITransactionRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SQL_CREATE =
            "INSERT INTO ET_TRANSACTIONS (TRANSACTION_ID, CATEGORY_ID, USER_ID, AMOUNT, NOTE, TRANSACTION_DATE) " +
            "VALUES(NEXTVAL('ET_TRANSACTIONS_SEQ'), ?, ?, ?, ?, ?)";
    private static final String SQL_FIND_BY_ID =
            "SELECT TRANSACTION_ID, CATEGORY_ID, USER_ID, AMOUNT, NOTE, TRANSACTION_DATE " +
            "FROM ET_TRANSACTIONS WHERE TRANSACTION_ID = ? AND CATEGORY_ID = ? AND USER_ID = ?";
    private static final String SQL_FIND_ALL =
            "SELECT TRANSACTION_ID, CATEGORY_ID, USER_ID, AMOUNT, NOTE, TRANSACTION_DATE " +
            "FROM ET_TRANSACTIONS WHERE CATEGORY_ID = ? AND USER_ID = ?";
    private static final String SQL_UPDATE =
            "UPDATE ET_TRANSACTIONS SET AMOUNT = ?, NOTE = ?, TRANSACTION_DATE = ? " +
            "WHERE USER_ID = ? AND CATEGORY_ID = ? AND TRANSACTION_ID = ?";
    private static final String SQL_DELETE =
            "DELETE FROM ET_TRANSACTIONS " +
            "WHERE USER_ID = ? AND CATEGORY_ID = ? AND TRANSACTION_ID = ?";
    private static final String SQL_DELETE_CATEGORY_IN_TRANSACTION =
            "DELETE FROM ET_TRANSACTIONS WHERE CATEGORY_ID = ?";

    private RowMapper<Transaction> mapTransactionFromDb(){
        return (rs, i) -> {
          return new Transaction(
                  rs.getInt("TRANSACTION_ID"),
                  rs.getInt("CATEGORY_ID"),
                  rs.getInt("USER_ID"),
                  rs.getDouble("AMOUNT"),
                  rs.getString("NOTE"),
                  rs.getLong("TRANSACTION_DATE")
          );
        };
    }

    private Transaction convertInsertOperationToTransaction(KeyHolder keyHolder){
        return new Transaction(
                (Integer) keyHolder.getKeys().get("TRANSACTION_ID"),
                (Integer) keyHolder.getKeys().get("CATEGORY_ID"),
                (Integer) keyHolder.getKeys().get("USER_ID"),
                Double.parseDouble( keyHolder.getKeys().get("AMOUNT").toString() ),
                keyHolder.getKeys().get("NOTE").toString(),
                Long.parseLong(keyHolder.getKeys().get("TRANSACTION_DATE").toString())

        );
    }

    @Override
    public Transaction createTransaction(int userId, int categoryId, double amount, String note, long transactionDate) {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);

                ps.setInt(1, categoryId);
                ps.setInt(2, userId);
                ps.setDouble(3, amount);
                ps.setString(4, note);
                ps.setLong(5, transactionDate);

                return ps;
            }, keyHolder);

            return convertInsertOperationToTransaction(keyHolder);

        }catch (Exception e){
            throw new BadRequestException("Invalid Transaction data");
        }
    }

    @Override
    public Transaction getTransactionById(int transactionId, int userId, int categoryId) {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_ID, mapTransactionFromDb(), transactionId, categoryId, userId);
        }catch (Exception e){
            throw new ResourceNotFoundException("transaction with the given id not found");
        }
    }

    @Override
    public List<Transaction> getAllTransactions(int userId, int categoryId) {
        return jdbcTemplate.query(SQL_FIND_ALL, mapTransactionFromDb() , categoryId, userId);
    }

    @Override
    public Transaction updateTransaction(int userId, int categoryId, int transactionId, Transaction transaction) {
        try{
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(conn -> {
                PreparedStatement ps = conn.prepareStatement(SQL_UPDATE, Statement.RETURN_GENERATED_KEYS);

                ps.setDouble(1, transaction.getAmount());
                ps.setString(2, transaction.getNote());
                ps.setLong(3, transaction.getTransactionDate());
                ps.setInt(4, userId);
                ps.setInt(5, categoryId);
                ps.setInt(6, transactionId);

                return ps;
            }, keyHolder);

            return convertInsertOperationToTransaction(keyHolder);

        }catch(Exception e){
            throw new BadRequestException("Invalid data for update transaction");
        }
    }

    @Override
    public void deleteTransactionById(int userId, int categoryId, int transactionId) {
        int row = jdbcTemplate.update(SQL_DELETE, userId, categoryId, transactionId);

        if (row == 0) { throw new ResourceNotFoundException("No transaction with the given Id, No transactions deleted");}
    }

    @Override
    public void deleteCategoryInsideTransactions(int categoryId){
        jdbcTemplate.update(SQL_DELETE_CATEGORY_IN_TRANSACTION, categoryId);
    }

}
