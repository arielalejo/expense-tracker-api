package com.ariel.expensetracker.repository;

import com.ariel.expensetracker.definitions.IUserRespository;
import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Objects;


@Repository("psqlUserRepository")
public class UserRepository implements IUserRespository {
    private static final String SQL_CREATE =
            "INSERT INTO ET_USERS(USER_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD) VALUES(NEXTVAL('ET_USERS_SEQ'), ?, ?, ?, ?)";
    private static final String SQL_COUNTS_IN_TABLE = "SELECT COUNT(*) FROM ET_USERS WHERE EMAIL = ?";
    private static final String SQL_FIND_BY_ID = "SELECT USER_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD FROM ET_USERS WHERE USER_ID = ?";
    private static final String SQL_FIN_BY_EMAIL = "SELECT USER_ID, FIRST_NAME, LAST_NAME, EMAIL, PASSWORD FROM ET_USERS WHERE EMAIL = ?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public User registerUser(String name, String lastName, String email, String password) throws AuthException {
        String hashedPassw = BCrypt.hashpw(password, BCrypt.gensalt(10));
        try{
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1,name);
                ps.setString(2, lastName);
                ps.setString(3, email);
                ps.setString(4, hashedPassw);

                return ps;
            }, keyHolder);

            int id = (Integer) keyHolder.getKeys().get("USER_ID");
            this.findById(id);
            User user = new User(name, lastName, email, password);
            user.setId(id);
            return user;

        }catch(Exception e){
            throw new AuthException("Invalid details. Failed to create account");
        }
    }

    @Override
    public User findByEmailAndPassword(String email, String password) {
        try{
            User user = jdbcTemplate.queryForObject(SQL_FIN_BY_EMAIL, userRowMapper, email);
            boolean isPasswordSame = BCrypt.checkpw(password, user.getPassword());
            if (!isPasswordSame) { throw new AuthException("invalid email or password"); }

            return user;
        }catch(EmptyResultDataAccessException e) {throw new AuthException("invalid email or password");}
    }

    @Override
    public User findById(int id) throws AuthException {
        try{
            return  jdbcTemplate.queryForObject(SQL_FIND_BY_ID, userRowMapper, id);
        } catch (Exception e) {
            throw new AuthException("Resourece with the given id not found");
        }
    }

    @Override
    public int emailIsRegisteredInTable(String email) {
        Integer res = jdbcTemplate.queryForObject(SQL_COUNTS_IN_TABLE, Integer.class, email);

        Objects.requireNonNull(res, "obj must be not null");
        return res;
    }

    private final RowMapper<User> userRowMapper= ((rs, rowNum) -> {
        int id = rs.getInt("USER_ID");
        String name =  rs.getString("FIRST_NAME");
        String lastName = rs.getString("LAST_NAME");
        String email = rs.getString("EMAIL");
        String password = rs.getString("PASSWORD");

        User u = new User(name, lastName, email, password); // ALL THE PARAMS IN THE CONSTRUCTOR
        u.setId(id);

        return u;
    });

}
