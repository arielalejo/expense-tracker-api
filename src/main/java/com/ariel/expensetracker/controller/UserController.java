package com.ariel.expensetracker.controller;

import com.ariel.expensetracker.definitions.IUserService;
import com.ariel.expensetracker.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController()
@RequestMapping("/api/users")
public class UserController {
    @Autowired()
    IUserService userService;

    @PostMapping("/register")
    public ResponseEntity<Map<String, String>> registerUser(@RequestBody User user){
        String userToken = userService.registerUser(user.getName(), user.getLastName(), user.getEmail(), user.getPassword());
        Map<String, String> userMap = new HashMap<String, String>(){{ put("token", userToken);}};

        return new ResponseEntity<Map<String, String>>(userMap, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> loginUser(@RequestBody Map<String, Object> user){
        String email = (String) user.get("email");
        String password = (String) user.get("password");

        String userToken = userService.validateUser(email, password);
        Map<String, String > userMap = new HashMap<String, String>(){{put("token", userToken);}};

        return new ResponseEntity<Map<String, String>>(userMap, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable("id") int id){
        return userService.getUserById(id);
    }

}
