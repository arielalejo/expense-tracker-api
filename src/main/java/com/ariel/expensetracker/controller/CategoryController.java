package com.ariel.expensetracker.controller;

import com.ariel.expensetracker.definitions.ICategoryService;
import com.ariel.expensetracker.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
    @Autowired
    ICategoryService categoryService;

    @GetMapping("")
    public ResponseEntity<List<Category>> getAllCategories(HttpServletRequest request){
        int userId = (Integer) request.getAttribute("userId");
        List<Category> categories = categoryService.getAllCategories(userId);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<Category> getCategoryById(@PathVariable("categoryId") Integer categoryId, HttpServletRequest request){
        int userId = (Integer) request.getAttribute("userId");
        Category category = categoryService.getCategoryById(userId, categoryId);

        return new ResponseEntity<>(category, HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<Category> createCategory(@RequestBody Map<String, Object>body, HttpServletRequest request){
        int userId = (Integer) request.getAttribute("userId");
        String title = (String) body.get("title");
        String description = (String) body.get("description");

        Category addedCategory = categoryService.createCategory(userId, title, description);
        return new ResponseEntity<Category>(addedCategory, HttpStatus.CREATED);
    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<Category> updateCategory(
            HttpServletRequest request, @PathVariable("categoryId") int categoryId, @RequestBody Category category){
        int userId = (Integer) request.getAttribute("userId");

        Category updatedCategory = categoryService.updateCategory(userId, categoryId, category);

        return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<Map<String, Boolean>> deleteCtruegory(
            HttpServletRequest request,
            @PathVariable("categoryId") int categoryId){

        int userId = (Integer) request.getAttribute("userId");
        categoryService.deleteCategofy(userId, categoryId);
        Map<String, Boolean> response = new HashMap<>(){{put("success", true);}};

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
