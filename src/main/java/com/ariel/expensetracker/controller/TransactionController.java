package com.ariel.expensetracker.controller;


import com.ariel.expensetracker.models.Transaction;
import com.ariel.expensetracker.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/categories/{categoryId}/transactions")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping("")
    public ResponseEntity<Transaction> createTransaction
            (HttpServletRequest request,
             @PathVariable("categoryId") int categoryId,
             @RequestBody Map<String, Object> transaction){

        int userId = (Integer) request.getAttribute("userId");
        double amount = Double.parseDouble(transaction.get("amount").toString());
        String note = (String) transaction.get("note");
        long transactionDate = Long.parseLong(transaction.get("transactionDate").toString());

        Transaction addedTransaction = transactionService.createTransaction(userId, categoryId, amount, note, transactionDate);

        return new ResponseEntity<>(addedTransaction, HttpStatus.OK);

    }

    @GetMapping("/{transactionId}")
    public ResponseEntity<Transaction> getTransactionById(
            HttpServletRequest request,
            @PathVariable("categoryId") int categoryId,
            @PathVariable("transactionId") int transactionId

    ){

        int userId = (Integer) request.getAttribute("userId");
        Transaction response = transactionService.getTransactionById(transactionId, userId, categoryId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<List<Transaction>> getAllTransactions(
            HttpServletRequest request,
            @PathVariable("categoryId") int categoryId){
        int userId = (Integer) request.getAttribute("userId");

        List<Transaction> allTransactions = transactionService.getAllTransactions(userId, categoryId);

        return new ResponseEntity<>(allTransactions, HttpStatus.OK);
    }

    @PutMapping("/{transactionId}")
    public ResponseEntity<Transaction> updateTransaction(
            HttpServletRequest request,
            @PathVariable("categoryId") int categoryId,
            @PathVariable("transactionId") int transactionId,
            @RequestBody Transaction transactionBody ){

        int userId = (Integer) request.getAttribute("userId");
        Transaction updatedTransaction = transactionService.updateTransaction(userId, categoryId, transactionId, transactionBody);

        return new ResponseEntity<>(updatedTransaction, HttpStatus.OK);

    }

    @DeleteMapping("/{transactionId}")
    public ResponseEntity<Map<String, Boolean>> deleteTransaction(
            HttpServletRequest request,
            @PathVariable("categoryId") int categoryId,
            @PathVariable("transactionId") int transactionId){

        int userId = (Integer) request.getAttribute("userId");

        transactionService.deleteTransactionById(userId, categoryId, transactionId);

        Map<String, Boolean> response = new HashMap<>(){{put("succes", true);}};
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
