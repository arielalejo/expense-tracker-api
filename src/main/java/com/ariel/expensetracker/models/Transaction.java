package com.ariel.expensetracker.models;

public class Transaction {
    private int transactionId;
    private int categoryId;
    private int userId;
    private double amount;
    private String note;
    private long transactionDate;

    public Transaction(int transactionId, int categoryId, int userId, double amount, String note, long transactionDate) {
        this.transactionId = transactionId;
        this.categoryId = categoryId;
        this.userId = userId;
        this.amount = amount;
        this.note = note;
        this.transactionDate = transactionDate;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getUserId() {
        return userId;
    }

    public double getAmount() {
        return amount;
    }

    public String getNote() {
        return note;
    }

    public long getTransactionDate() {
        return transactionDate;
    }
}
