package com.ariel.expensetracker.models;

public class Category {
    private int categoryId;
    private int userId;
    private String title;
    private String description;
    private Double totalExpese;

    public Category(int categoryId, int userId, String title, String description, Double totalExpese) {
        this.categoryId = categoryId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.totalExpese = totalExpese;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Double getTotalExpese() {
        return totalExpese;
    }
}
