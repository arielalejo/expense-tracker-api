package com.ariel.expensetracker.services;

import com.ariel.expensetracker.definitions.ICategoryRepository;
import com.ariel.expensetracker.definitions.ICategoryService;
import com.ariel.expensetracker.definitions.ITransactionRepository;
import com.ariel.expensetracker.exceptions.BadRequestException;
import com.ariel.expensetracker.exceptions.ResourceNotFoundException;
import com.ariel.expensetracker.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements ICategoryService {
    @Autowired
    ICategoryRepository categoryRepository;

    @Autowired
    ITransactionRepository transactionRepository;

    @Override
    public List<Category> getAllCategories(int userId) {

        return categoryRepository.getAllCategories(userId);
    }

    @Override
    public Category getCategoryById(int userId, int categoryId) throws ResourceNotFoundException {
        return categoryRepository.getCategoryById(userId, categoryId);
    }

    @Override
    public Category createCategory(int userId, String title, String description) throws BadRequestException {
        int addCategoryId = categoryRepository.createCategory(userId, title,description);
        return new Category(addCategoryId, userId, title, description, 0.0);
    }

    @Override
    public Category updateCategory(int userId, int categoryId, Category category) throws BadRequestException {
        return categoryRepository.updateCategory(userId, categoryId, category);
    }

    @Override
    public void deleteCategofy(int userId, int categoryId) throws ResourceNotFoundException {
        transactionRepository.deleteCategoryInsideTransactions(categoryId);
        categoryRepository.deleteCategory(userId, categoryId);
    }
}
