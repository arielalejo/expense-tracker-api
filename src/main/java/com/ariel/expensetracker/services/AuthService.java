package com.ariel.expensetracker.services;

import com.ariel.expensetracker.constants.Constants;
import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

import java.util.Date;


public class AuthService {
    public static Algorithm algorithm = Algorithm.HMAC256(Constants.JWT_PRIVATE_KEY);

    public static String generateToken(User user){
        long timestamp = System.currentTimeMillis();

        try {
            String token = JWT.create()
                    .withClaim("id", user.getId() )
                    .withClaim("name", user.getName())
                    .withClaim("lastName", user.getLastName())
                    .withClaim("email", user.getEmail())
//                    .withIssuedAt(new Date(timestamp))
//                    .withExpiresAt(new Date(timestamp + Constants.JWT_TOKEN_VALIDITY))
                    .sign(AuthService.algorithm);

            return token;
        }catch (JWTCreationException e){
            throw new AuthException("token service error");
        }

    }
}
