package com.ariel.expensetracker.services;

import com.ariel.expensetracker.definitions.IUserRespository;
import com.ariel.expensetracker.definitions.IUserService;
import com.ariel.expensetracker.exceptions.AuthException;
import com.ariel.expensetracker.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service()
@Transactional()
public class UserService implements IUserService {
    @Autowired()
    @Qualifier("psqlUserRepository") IUserRespository userRepository;

    @Override
    public String registerUser(String name, String lastName, String email, String password) throws AuthException {
        if (email == null)  throw  new AuthException("no email provide");

        email= email.toLowerCase();
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        Matcher matcher = pattern.matcher(email);
        if(!matcher.matches()) throw new AuthException("email incorrect");

        int count = userRepository.emailIsRegisteredInTable(email);
        if (count > 0) throw new AuthException("email already exists");

        User registeredUser = userRepository.registerUser(name, lastName, email, password);
        return AuthService.generateToken(registeredUser);
    }

    @Override
    public String validateUser(String email, String password) throws AuthException {
        email = email.toLowerCase();

        User user = userRepository.findByEmailAndPassword(email, password);
        return AuthService.generateToken(user);


    }

    public User getUserById(int id){
        return userRepository.findById(id);
    }
}
