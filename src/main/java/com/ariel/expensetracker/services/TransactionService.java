package com.ariel.expensetracker.services;

import com.ariel.expensetracker.definitions.ITransactionService;
import com.ariel.expensetracker.models.Transaction;
import com.ariel.expensetracker.repository.CategoryRepository;
import com.ariel.expensetracker.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TransactionService implements ITransactionService {
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Transaction createTransaction(int userId, int categoryId, double amount, String note, long transactionDate) {
        return transactionRepository.createTransaction(userId, categoryId, amount, note, transactionDate);

    }

    @Override
    public Transaction getTransactionById(int transactionId, int userId, int categoryId) {
        categoryRepository.getCategoryById(userId, categoryId);

        return transactionRepository.getTransactionById(transactionId, userId, categoryId);
    }

    @Override
    public List<Transaction> getAllTransactions(int userId, int categoryId) {
        categoryRepository.getCategoryById(userId, categoryId);

        return transactionRepository.getAllTransactions(userId, categoryId);
    }

    @Override
    public Transaction updateTransaction(int userId, int categoryId, int transactionId, Transaction transaction) {
        return transactionRepository.updateTransaction(userId, categoryId, transactionId, transaction);
    }

    @Override
    public void deleteTransactionById(int userId, int categoryId, int transactionId) {
        transactionRepository.deleteTransactionById(userId, categoryId, transactionId);
    }
}
